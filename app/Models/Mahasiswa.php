<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;

    protected $table = "mahasiswa";
    protected $fillable = ["npm", "name_mahasiswa", "fakultas", "prodi"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
