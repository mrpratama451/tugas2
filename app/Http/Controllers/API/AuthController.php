<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;

class AuthController extends Controller
{
    public function register (Request $request)
    {
        // validator
        $validator = Validator::make($request->all(), [
            'name'=> 'required',
            'email'=> 'required|email',
            'password'=> 'required',
            'confirm_password'=> 'required|same:password',
        ]);

        // cek validator (false)
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Ada Kesalahan',
                'data' => $validator->errors()
            ]);
        }
        
        // cek validator (true)
        $input = $request->all();
        $input['password'] = bcrypt ($input['password']); // hash password
        $user = User::create($input); // input to db

        // buat token 
        $success['token'] = $user->createToken('auth_token')->plainTextToken;
        $success['name'] = $user->name;

        return response()->json([
            'success' => true,
            'message' => 'Registrasi Sukses',
            'data' => $success
        ]);
    }

    public function login (Request $request)
    {
        // cek email dan pass
        if (Auth::attempt (['email' => $request->email, 'password' => $request->password])) {
            $auth = Auth::user();
            $success['token'] = $auth->createToken('auth_token')->plainTextToken;
            $success['name'] = $auth->name;
            $success['email'] = $auth->email;

            return response()->json([
                'success' => true,
                'message' => 'Login Berhasil',
                'data' => $success
            ]);
        }else {
            return response()->json([
                'success' => false,
                'message' => 'Login gagal, Harap cek email dan password',
                'data' => null
            ]);
        }
    }
    
    public function logout (Request $request)
    {
        $user = $request->user();
        $user->currentAccessToken()->delete();
        return response()->json([
                'success' => true,
                'message' => 'Berhasil Logout',
                'data' => null
            ]);
    }
}
