<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Helpers\ApiFormatter;
use Illuminate\Http\Request;
use Exception;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Mahasiswa::all();
        
        if ($data) {
            return ApiFormatter::createApi(200, 'Success', $data);
        } else {
            return ApiFormatter::createApi(400, 'Failed');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dikosongkan karna ini untuk tampilan setelah diproses 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try ... catch ... error handling laravel
        try {
            // validasi 
            $request->validate([
                'npm' => 'required',
                'name_mahasiswa' => 'required',
                'fakultas' => 'required',
                'prodi' => 'required',
            ]);

            $mahasiswa = Mahasiswa::create([
                'npm' => $request->npm,
                'name_mahasiswa' => $request->name_mahasiswa,
                'fakultas' => $request->fakultas,
                'prodi' => $request->prodi,
            ]);

            $data = Mahasiswa::where('id', '=', $mahasiswa->id)->get();

            if ($data) {
                return ApiFormatter::createApi(200, 'Success', $data);
            } else {
                return ApiFormatter::createApi(400, 'Failed');
            }
        } catch (Exception $error) {
            return ApiFormatter::createApi(400, 'Failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Mahasiswa::find($id);

        if ($data) {
            return ApiFormatter::createApi(200, 'Success', $data);
        } else {
            return ApiFormatter::createApi(400, 'Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dikosongkan karna ini untuk tampilan setelah diproses 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'npm' => 'required',
            'name_mahasiswa' => 'required',
            'fakultas' => 'required',
            'prodi' => 'required',
        ]);

        $data = Mahasiswa::find($id);
        
        $data->npm = $request->npm;
        $data->name_mahasiswa = $request->name_mahasiswa;
        $data->fakultas = $request->fakultas;
        $data->prodi = $request->prodi;
        
        $data->save();

        if ($data) {
            return ApiFormatter::createApi(200, 'Success', $data);
        } else {
            return ApiFormatter::createApi(400, 'Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = mahasiswa::find($id);
        $data->delete();
        
         if ($data) {
            return ApiFormatter::createApi(200, 'Success Delete Data');
        } else {
            return ApiFormatter::createApi(400, 'Failed');
        }
    }
}
