<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\mahasiswaController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// crud mahasiswa

// Route::group(['middleware' => ['auth:sanctum']], function () {
    
//     Route::get('/mahasiswa',[mahasiswaController::class,'index']);
//     Route::get('/mahasiswa/create',[mahasiswaController::class,'create']);
//     Route::post('/mahasiswa',[mahasiswaController::class,'store']);
//     Route::get('/mahasiswa/{mahasiswa_id}',[mahasiswaController::class,'show']);
//     Route::get('/mahasiswa/{mahasiswa_id}/edit',[mahasiswaController::class,'edit']);
//     Route::put('/mahasiswa/{mahasiswa_id}',[mahasiswaController::class,'update']);
//     Route::delete('/mahasiswa/{mahasiswa_id}',[mahasiswaController::class,'destroy']);
// });

// crud mahasiswa