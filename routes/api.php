<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\MahasiswaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// regis, login, get user
Route::post('register',[AuthController::class,'register']);
Route::post('login',[AuthController::class,'login']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user(); //get info user
});
// regis, login, get user

Route::group(['middleware' => ['auth:sanctum']], function () {

    // crud
        Route::get('/mahasiswa', [MahasiswaController::class, 'index']);
        Route::post('/mahasiswa/store', [MahasiswaController::class, 'store']);
        Route::get('/mahasiswa/show/{id}', [MahasiswaController::class, 'show']);
        Route::post('/mahasiswa/{id}/edit', [MahasiswaController::class, 'update']);
        Route::get('/mahasiswa/destroy/{id}', [MahasiswaController::class, 'destroy']);
    // crued

    Route::post('logout', [AuthController::class,'logout']);
});

